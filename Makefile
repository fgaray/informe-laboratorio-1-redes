NOMBRE_TEX=prueba


all: $(NOMBRE_TEX).pdf

$(NOMBRE_TEX).pdf: $(NOMBRE_TEX).tex memoria.cls bibliografia.bib
	pdflatex -shell-escape $(NOMBRE_TEX).tex
	biber $(NOMBRE_TEX) 
	pdflatex -shell-escape $(NOMBRE_TEX).tex
	biber $(NOMBRE_TEX) 
	pdflatex -shell-escape $(NOMBRE_TEX).tex

clear:
	rm $(NOMBRE_TEX).pdf $(NOMBRE_TEX).bbl $(NOMBRE_TEX).aux $(NOMBRE_TEX).bcf $(NOMBRE_TEX).blg $(NOMBRE_TEX).lof $(NOMBRE_TEX).log $(NOMBRE_TEX).lot  $(NOMBRE_TEX).run.xml  $(NOMBRE_TEX).toc

